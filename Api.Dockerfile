FROM node:12-alpine as builder
WORKDIR /source
COPY package.json package-lock.json ./
RUN npm i 
COPY tsconfig.json ./
COPY ./src ./src
RUN npm run build

FROM jwilder/dockerize:0.6.1 as dockerize

FROM node:12-alpine as app
COPY package.json package-lock.json ./
RUN npm i --production
COPY --from=builder /source/build /app
COPY --from=dockerize /usr/local/bin /usr/local/bin
CMD [ "node", "/app/server.js" ]