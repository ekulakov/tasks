/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
    pgm.createTable('tasks', {
        id: 'id',
        name: { type: 'varchar(255)', notNull: true },
        priority: { type: 'smallint', notNull: true }
    });
    pgm.createIndex('tasks', 'priority');
};

exports.down = (pgm) => {
    pgm.dropIndex('tasks', 'priority');
    pgm.dropTable('tasks');
};
