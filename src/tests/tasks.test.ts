import request from "supertest";

import app from '../app';
import db from '../db';

beforeEach(async () => {
    await db.query("DELETE FROM tasks");
});

describe('/tasks endpoint', () => {

    it('it should return an empty response if there are no tasks', async () => {
        const res = await request(app)
            .get('/tasks');

        expect(res.body).toBe("");
    });

    it('it should create a new task', async () => {
        const res = await request(app)
            .post('/tasks')
            .send({
                name: "New task",
                priority: 10
            });

        const containing = expect.objectContaining({ name: "New task", priority: 10 });
        expect(res.body).toEqual(containing);
    });

    it('it should return an added task', async () => {
        await request(app)
            .post('/tasks')
            .send({
                name: "New task",
                priority: 20
            });

        const res = await request(app)
            .get('/tasks');

        const containing = expect.objectContaining({ name: "New task", priority: 20 });
        expect(res.body).toEqual(containing);
    });

    it('it should return a task which has max priority', async () => {
        await request(app)
            .post('/tasks')
            .send({
                name: "New task",
                priority: 30
            });

        await request(app)
            .post('/tasks')
            .send({
                name: "New task",
                priority: 20
            });

        const res = await request(app)
            .get('/tasks');

        const containing = expect.objectContaining({ name: "New task", priority: 30 });
        expect(res.body).toEqual(containing);
    });

    it('it should delete a task', async () => {
        const created = await request(app)
            .post('/tasks')
            .send({
                name: "New task",
                priority: 30
            });

        const res = await request(app)
            .delete(`/tasks/${created.body.id}`);

        expect(res.status).toBe(200);
    });

    it('it should return 404 if task is not found', async () => {
        const res = await request(app)
            .delete(`/tasks/111`);

        expect(res.status).toBe(404);
    });

    it('it should return 400 if task is invalid', async () => {
        const res = await request(app)
            .post('/tasks')
            .send({
                name: "New task"
            });

        expect(res.status).toBe(400);
    });

    it('it should validate a task', async () => {
        const res = await request(app)
            .post('/tasks')
            .send({
                name: "New task"
            });

        expect(res.body).toEqual([{
            dataPath: "",
            keyword: "required",
            message: "should have required property 'priority'",
            params: {
                missingProperty: "priority"
            },
            schemaPath: "#/required"
        }]);
    });
});
