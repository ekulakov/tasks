import { crateValidator } from "./validator";
import { Task } from "../models/task";
import taskSchema from '../schemas/task.json';

const validator = crateValidator<Task>(taskSchema);

export default validator;