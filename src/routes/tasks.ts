import { Request, Response, Router } from "express";
import asyncHandler from "express-async-handler"
import taskValidator from "../validators/taskValidator";
import * as TasksService from '../services/tasks';

const getTask = async (req: Request, res: Response) => {
    const task = await TasksService.getTask();
    res.json(task);
};

const addTask = async (req: Request, res: Response) => {
    if (!taskValidator.isValid(req.body)) {
        const errors = taskValidator.getErrors();
        res.status(400).json(errors);
        return;
    }

    const task = await TasksService.addTask(req.body);
    res.json(task);
};

const deleteTask = async (req: Request, res: Response) => {
    const { id } = req.params;
    const deleted = await TasksService.deleteTask(+id);

    if (deleted) {
        res.sendStatus(200);
    } else {
        res.sendStatus(404);
    }
};

const router = Router();

router.get('/tasks', asyncHandler(getTask));
router.post('/tasks', asyncHandler(addTask));
router.delete('/tasks/:id', asyncHandler(deleteTask));

export default router;