import db from "../db";
import { Task } from "../models/task";

export async function addTask(task: Task): Promise<Task> {
    const { rows } = await db.query(
        'INSERT INTO tasks (name, priority) VALUES($1, $2) RETURNING *',
        [task.name, task.priority]);

    return rows[0];
}

export async function getTask(): Promise<Task> {
    const { rows } = await db.query('SELECT id, name, priority FROM tasks ORDER BY priority DESC LIMIT 1');
    return rows[0];
}

export async function deleteTask(id: number): Promise<boolean> {
    const { rowCount } = await db.query('DELETE FROM tasks WHERE id = $1', [id]);
    return !!rowCount;
}
