import app from "./app";

app.listen(process.env.PORT, function () {
    console.log(`Tasks API listening on port ${process.env.PORT}!`);
});