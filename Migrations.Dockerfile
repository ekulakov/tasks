FROM jwilder/dockerize:0.6.1 as dockerize
FROM node:12-alpine
COPY package.json package-lock.json ./
RUN npm i --production
COPY ./migrations ./migrations
COPY --from=dockerize /usr/local/bin /usr/local/bin
CMD ["npm", "run", "migrate", "up"]